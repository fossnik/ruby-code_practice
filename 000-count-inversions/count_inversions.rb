def countInversionsNaive(inputArray)
  inversions_count = 0
  inputArray.each_with_index do | i, index_i |
    inputArray.each_with_index do | j, index_j |
      if i < j && index_i < index_j
        inversions_count += 1
      end
    end
  end
  inversions_count
end

