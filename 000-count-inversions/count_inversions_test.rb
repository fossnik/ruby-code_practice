require 'minitest/autorun'
require_relative 'count_inversions'

class CountInversionsTest < Minitest::Test
  def test_1
    assert_equal 4, countInversionsNaive([1, 3, 2, 0])
  end

  def test_2
    assert_equal 4, countInversionsNaive([1, 4, 10, 4, 2])
  end
end