require 'minitest/autorun'
require_relative 'quasifactorial'

# Common test data version: 1.7.0 cacf1f1
class QuasifactorialTest < Minitest::Test
  def test_1
    # skip
    assert_equal 7, Quasifactorial.quasifactorial(4)
  end

  def test_2
    # skip
    assert_equal 1, Quasifactorial.quasifactorial(1)
  end

  def test_3
    # skip
    assert_equal 1, Quasifactorial.quasifactorial(2)
  end
end
