class Quasifactorial
  def self.quasifactorial(n)
    if n > 1
      n * quasifactorial(n - 1) - 1
    else
      1
    end
  end
end
