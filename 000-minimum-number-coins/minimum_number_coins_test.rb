require 'minitest/autorun'
require_relative 'minimum_number_coins'

class MinimumNumberCoinsTest < Minitest::Test
  def test_1
    assert_equal 6, minimalNumberOfCoins([1, 2, 10], 28)
  end

  def test_2
    assert_equal 10, minimalNumberOfCoins([1, 5, 10, 100], 239)
  end

  def test_3
    assert_equal 8, minimalNumberOfCoins([1], 8)
  end

  def test_4
    assert_equal 7, minimalNumberOfCoins([1, 2, 20, 60, 120], 150)
  end

  def test_5
    assert_equal 4, minimalNumberOfCoins([1, 3, 6, 30], 45)
  end
end