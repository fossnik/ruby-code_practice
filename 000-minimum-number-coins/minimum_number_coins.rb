def minimalNumberOfCoins(coins, price)
  num_coins = 0
  sum = 0
  coins.sort.reverse.each do | coin_value |
    goes_into = (price - sum) / coin_value
    sum += goes_into * coin_value
    num_coins += goes_into
    return num_coins if sum == price
  end
  num_coins
end
