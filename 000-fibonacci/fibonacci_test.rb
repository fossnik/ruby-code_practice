require 'minitest/autorun'
require_relative 'fibonacci'

# Common test data version: 1.7.0 cacf1f1
class FibonacciTest < Minitest::Test
  def test_1
    # skip
    assert_equal 1, Fibonacci.compute(2)
  end

  def test_2
    # skip
    assert_equal 1, Fibonacci.compute(1)
  end

  def test_3
    # skip
    assert_equal 13, Fibonacci.compute(7)
  end

  def test_4
    # skip
    assert_equal 8, Fibonacci.compute(6)
  end

  def test_5
    # skip
    assert_equal 3, Fibonacci.compute(4)
  end

  def test_6
    # skip
    assert_equal 5, Fibonacci.compute(5)
  end

  def test_7
    # skip
    assert_equal 233, Fibonacci.compute(13)
  end

  def test_8
    # skip
    assert_equal 2, Fibonacci.compute(3)
  end
end
