class Fibonacci
  def self.compute(n)
    n <= 1 ? n : compute(n - 1) + compute(n - 2)
  end
end