class SwapNeighboringDigits
  def self.swap(n)
    array = n.to_s.split('')

    index = 0
    while index < array.length
      temp = array[index]
      array[index] = array[index + 1]
      array[index + 1] = temp
      index += 2
    end
    array.join.to_i
  end

end