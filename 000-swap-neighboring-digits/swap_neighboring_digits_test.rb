require 'minitest/autorun'
require_relative 'swap_neighboring_digits'

# Common test data version: 1.7.0 cacf1f1
class SwapNeighboringDigitsTest < Minitest::Test
  def test_1
    # skip
    assert_equal 2143, SwapNeighboringDigits.swap(1234)
  end

  def test_2
    # skip
    assert_equal 12345678, SwapNeighboringDigits.swap(21436587)
  end

  def test_3
    # skip
    assert_equal 12, SwapNeighboringDigits.swap(21)
  end
end
