def factorialTrailingZeros(n)
  def fac(n)
    n == 1 ? 1 : n * fac(n - 1)
  end

  fac(n).to_s.reverse.index(/[^0]/)
end
