require 'minitest/autorun'
require_relative 'factorial_trailing_zeros'

class MinimumNumberCoinsTest < Minitest::Test
  def test_1
    assert_equal 2, factorialTrailingZeros(10)
  end

  def test_2
    assert_equal 6, factorialTrailingZeros(29)
  end

  def test_3
    assert_equal 7, factorialTrailingZeros(31)
  end
end