def swapDiagonals(matrix)
  matrix.each_with_index.map do | row, row_index |
    row.each_with_index.map do | element, col_index |
      if col_index == row_index
        row[row.length - row_index - 1]
      elsif  col_index == row.length - row_index - 1
        row[row_index]
      else
        element
      end
    end
  end
end
